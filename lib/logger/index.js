const winston = require('winston');
const config = require('../config');
const consoleFormat = require('./formats/console');
const timestampFormat = require('./formats/timestamp');
const stripMergedSplatFormat = require('./formats/stripMergedSplat');

const logger = winston.createLogger({
    format: winston.format.combine(
        stripMergedSplatFormat(),
        timestampFormat(),
        winston.format.errors({ stack: true }),
    ),
    transports: [
        new winston.transports.Console({
            level: config.get('log:console_level'),
            format: winston.format.combine(
                winston.format.colorize({ all: true }),
                winston.format.padLevels(),
                consoleFormat({
                    showMeta: true,
                    metaStrip: ['timestamp', 'name'],
                    inspectOptions: {
                        depth: Infinity,
                        colors: true,
                        maxArrayLength: Infinity,
                        breakLength: 120,
                        compact: Infinity,
                    },
                }),
            ),
        }),
    ],
});

process.on('unhandledRejection', e => logger.error(e, { unhandledPromiseRejection: true }));

module.exports = logger;
