const winston = require('winston');
const { SPLAT } = require('triple-beam');

const stripMergedSplat = winston.format(info => {
    if (info[SPLAT]) {
        const [meta] = info[SPLAT];
        if (typeof meta === 'object' && meta !== null) {
            Object.keys(meta).forEach(key => delete info[key]);
        }
    }

    return info;
});

module.exports = stripMergedSplat;
