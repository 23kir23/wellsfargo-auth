const winston = require('winston');

const timestamp = winston.format(info => {
    info.timestamp = new Date().toISOString().slice(0, 23).replace('T', ' ');

    return info;
});

module.exports = timestamp;
