const { LEVEL, MESSAGE, SPLAT } = require('triple-beam');
const { inspect } = require('util');
const colors = require('colors/safe');

class Console {
    static reSpaces = /^\s+/;

    static reSpacesOrEmpty = /^(\s*)/;

    static reColor = /\x1B\[\d+m/;

    static defaultStrip = [LEVEL, MESSAGE, SPLAT, 'level', 'message', 'ms', 'stack'];

    static chars = {
        singleLine: '▪',
        startLine: '┏',
        line: '┃',
        endLine: '┗',
    };

    constructor(opts = {}) {
        this.opts = opts;
        if (typeof this.opts.showMeta === 'undefined') {
            this.opts.showMeta = true;
        }
    }

    inspector(value, messages) {
        const inspector = inspect(value, this.opts.inspectOptions || {});
        inspector.split('\n').forEach(line => {
            messages.push(line);
        });
    }

    message(info, chr, color) {
        const message = info.message.replace(Console.reSpacesOrEmpty, `$1${color}${colors.dim(chr)}${colors.reset(' ')}`);
        return `${info.timestamp} ${info.level}:${message}`;
    }

    pad(message) {
        let spaces = '';
        const matches = message && message.match(Console.reSpaces);
        if (matches && matches.length > 0) {
            spaces = matches[0];
        }
        return spaces;
    }

    ms(info) {
        let ms = '';
        if (info.ms) {
            ms = colors.italic(colors.dim(` ${info.ms}`));
        }
        return ms;
    }

    stack(info) {
        const messages = [];
        if (info.stack) {
            const error = new Error();
            error.stack = info.stack;
            this.inspector(error, messages);
        }
        return messages;
    }

    meta(info) {
        const messages = [];
        const stripped = { ...info };
        Console.defaultStrip.forEach(e => delete stripped[e]);
        this.opts.metaStrip && this.opts.metaStrip.forEach(e => delete stripped[e]);
        if (Object.keys(stripped).length > 0) {
            this.inspector(stripped, messages);
        }
        return messages;
    }

    splat(info) {
        const messages = [];

        const splat = info[SPLAT];
        if (splat) {
            this.inspector(splat, messages);
        }

        return messages;
    }

    getColor(info) {
        let color = '';
        const colorMatch = info.level.match(Console.reColor);
        if (colorMatch) {
            color = colorMatch[0];
        }
        return color;
    }

    write(info, messages, color) {
        const pad = this.pad(info.message);
        messages.forEach((line, index, arr) => {
            const lineNumber = colors.dim(`[${(index + 1).toString().padStart(arr.length.toString().length, ' ')}]`);
            let chr = Console.chars.line;
            if (index === arr.length - 1) {
                chr = Console.chars.endLine;
            }
            info[MESSAGE] += `\n${info.timestamp} ${colors.dim(info.level)}:${pad}${color}${colors.dim(chr)}${colors.reset(' ')}`;
            info[MESSAGE] += `${lineNumber} ${line}`;
        });
    }

    transform(info) {
        const messages = [];
        if (this.opts.showMeta) {
            messages.push(...this.splat(info));
            messages.push(...this.stack(info));
            messages.push(...this.meta(info));
        }
        const color = this.getColor(info);
        info[MESSAGE] = this.message(info, Console.chars[messages.length > 0 ? 'startLine' : 'singleLine'], color);
        info[MESSAGE] += this.ms(info);
        this.write(info, messages, color);
        return info;
    }
}

module.exports = opts => {
    return new Console(opts);
};
