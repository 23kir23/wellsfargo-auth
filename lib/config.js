/* eslint-disable class-methods-use-this */
const nconf = require('nconf');
const { resolve } = require('path');

class Config {
    constructor() {
        nconf.argv();

        this._loadEnv();
        this._loadFiles();

        nconf.defaults({
            environment: 'development',
        });
    }

    _loadEnv() {
        nconf.env({
            separator: '__',
            lowerCase: true,
            parseValues: true,
            transform(obj) {
                if (obj.key === 'node_env') {
                    obj.key = 'environment';
                }

                return obj;
            },
        });
    }

    _loadFiles() {
        nconf.file('local', resolve('config.local.json'));

        if (this.isProduction) {
            nconf.file('environment', resolve('config.prod.json'));
        }

        nconf.file('basic', resolve('config.json'));
    }

    get(key) {
        return nconf.get(key);
    }

    set(key, value) {
        return nconf.set(key, value);
    }

    get isProduction() {
        return this.get('environment') === 'production';
    }
}

module.exports = new Config();
