const WellsfargoLoginService = require('src/services/WellsfargoLogin');

async function login(req, res) {
    const { companyId, userId, password } = req.body;

    if (!companyId /*|| !userId*/ || !password) {
        res.status(400).send('Incorrect parameters');
        return;
    }

    const WellsfargoLogin = new WellsfargoLoginService(companyId, userId, password);
    const cookies = await WellsfargoLogin.login();

    res.json({ data: cookies });
}

module.exports = login;
