const express = require('express');
require('express-async-errors');
const bodyParser = require('body-parser');
const http = require('http');

const app = express();
app.use(bodyParser.json());

app.post('/login', require('./controllers/login'));

// eslint-disable-next-line no-unused-vars
app.use((e, req, res, next) => {
    logger.error(e);
    res.status(500).send(e);
});

const server = http.createServer(app);
server.listen(config.get('server:port'), () => {
    logger.info(`Listening on *:${config.get('server:port')}`);
});
