const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');

puppeteer.use(StealthPlugin());

class WellsfargoLogin {
    static URL = 'https://wellsoffice.ceo.wellsfargo.com/portal/signon/index.jsp#/';
    // static URL = 'https://www.autobidmaster.com/en/login';

    constructor(companyId, userId, password) {
        this._companyId = companyId;
        this._userId = userId;
        this._password = password;
    }

    async login() {
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                // Required for Docker version of Puppeteer
                '--no-sandbox',
                '--disable-setuid-sandbox',
                // This will write shared memory files into /tmp instead of /dev/shm,
                // because Docker’s default for /dev/shm is 64MB
                '--disable-dev-shm-usage',
            ],
        });

        const page = await browser.newPage();
        await page.setViewport({ width: 1200, height: 720 });

        await page.goto(WellsfargoLogin.URL, { waitUntil: 'networkidle0' });

        await page.type('form#loginForm input[name=companyIdField]', this._companyId, { delay: 100 });
        await page.type('form#loginForm input[name=userIdField]', this._userId, { delay: 100 });
        await page.type('form#loginForm input[name=passwordField]', this._password, { delay: 100 });

        // await page.type('form#login_form input[name=login]', this._companyId, { delay: 100 });
        // await page.type('form#login_form input[name=password]', this._password, { delay: 100 });

        await Promise.all([
            page.click('form#loginForm button[type=submit]'),
            // page.click('form#login_form button[type=submit]'),
            page.waitForNavigation({ waitUntil: 'networkidle0' }),
        ]);

        // await page.screenshot({ path: 'result.png' });

        const cookies = await page.cookies();

        await browser.close();

        return cookies.map(cookie => `${cookie.name}=${cookie.value}`).join('; ');
    }
}

module.exports = WellsfargoLogin;
