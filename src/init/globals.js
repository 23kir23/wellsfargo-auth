process.env.NODE_PATH = process.cwd();
require('module').Module._initPaths();

global.config = require('lib/config');
global.logger = require('lib/logger');
global.isProduction = global.config.isProduction;
